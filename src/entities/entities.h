/**
 * Base/template application for SDL Games.
 * 
 * Reference : http://www.sdltutorials.com/sdl-entities
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#ifndef _ENTITIES_H_
#define _ENTITIES_H_

#include <SDL/SDL.h>
// Get our helper services from a separate library.
#include "../../lib/SDL_helper/animate.h"
#include "../../lib/SDL_helper/entity.h"
#include "../../lib/SDL_helper/event.h"
#include "../../lib/SDL_helper/surface.h"

class app : public CEvent {
	public:
		// Initialize our local/game variables.
		app() {
			display = NULL;

			running = true;
		}
		/**
		 * Primary / shell functionality (common to all/most games).
		 */
		// Start the main loop for the game itself.
		int execute();
		// Initialize all our resources (including SDL).
		bool init();
		// Process events through our SDL_helper common event handler.
		void handle_event(SDL_Event *event) {
			CEvent::OnEvent(event);
		}
			// Override our predefined (virtual) events.
			void OnExit();
		// Loop through each of our entity running their contained loop methods.
		void loop() {
			for(int i = 0; i < entity::EntityList.size(); i++) {
				if(!entity::EntityList[i]) continue;

				entity::EntityList[i]->loop();
			}
		}
		// Render our final display.
		void render();
		// Cleanup/free any resources.
		void cleanup();

	private:
		/**
		 * Primary / shell functionality (common to all/most games).
		 */
		// Track program status.
		bool running;
		// Primary display.
		SDL_Surface *display;
		/**
		 * Local functions.
		 */
		// Some basic entities (sprites if you like).
		entity Entity1;
		entity Entity2;
};

#endif // _ENTITIES_H_
