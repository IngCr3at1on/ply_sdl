/**
 * Base/template application for SDL Games.
 * 
 * Reference : http://www.sdltutorials.com/sdl-entities
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#include "entities.h"

bool app::init() {
	if(SDL_Init(SDL_INIT_EVERYTHING) < 0) {
		return false;
	}
	if((display = SDL_SetVideoMode(64, 64, 32, SDL_HWSURFACE | SDL_DOUBLEBUF)) == NULL) {
		return false;
	}
	if(Entity1.new_entity_from_bitmap("../res/image/yoshi.bmp", 64, 64, 8) == false) {
		return false;
	}
	if(Entity2.new_entity_from_bitmap("../res/image/yoshi.bmp", 64, 64, 8) == false) {
		return false;
	}
/* Returns true and the program runs but we still have pink.
 * 
	} else if (CSurface::SCChanger(Surf_Image, 255, 0, 255, 0, 0, 0) == false) {
		/* Change pink to black using the new SolidColorChanger method.
		 * return false is unsuccessful. *
		 return false;
	}
*/

	entity::EntityList.push_back(&Entity1);
	entity::EntityList.push_back(&Entity2);

	/* Finally return true, stating that all of our resources have properly
	 * initialized. */
	return true;
}
