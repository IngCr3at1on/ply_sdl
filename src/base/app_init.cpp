/**
 * Base/template application for SDL Games.
 * 
 * Reference : http://www.sdltutorials.com/
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#include "app.h"

bool app::init() {
	if(SDL_Init(SDL_INIT_EVERYTHING) < 0) {
		return false;
	}
	if((display = SDL_SetVideoMode(600, 600, 32, SDL_HWSURFACE | SDL_DOUBLEBUF)) == NULL) {
		return false;
	}
	if((image = surface::new_surface("../res/image/17.png")) == NULL) {
		return false;
	}

	/* Finally return true, stating that all of our resources have properly
	 * initialized. */
    return true;
}
