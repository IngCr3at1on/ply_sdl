/**
 * Simple SDL based map; based off of SDL Tutorials.
 * 
 * Reference : http://www.sdltutorials.com/sdl-maps
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#include "maps.h"
/**
 * General overrides.
 */
// Basic close function.
void app::OnExit() {
	running = false;
}

// Track the camera location by keyboard movement.
void app::OnKeyDown(SDLKey sym, SDLMod mod, Uint16 unicode) {
	switch(sym) {
		case SDLK_UP:
			CCamera::CameraControl.OnMove(0, 5);
			break;
		case SDLK_DOWN:
			CCamera::CameraControl.OnMove(0, -5);
			break;
		case SDLK_LEFT:
			CCamera::CameraControl.OnMove(5, 0);
			break;
		case SDLK_RIGHT:
			CCamera::CameraControl.OnMove(-5, 0);
			break;
		default:
			break;
	}
}
