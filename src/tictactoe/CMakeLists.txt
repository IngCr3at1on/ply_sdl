set(SOURCES
	tictactoe.h
	tictactoe.cpp
	ttt_cleanup.cpp
	ttt_events.cpp
	ttt_init.cpp
	ttt_render.cpp
)

add_executable(tictactoe ${SOURCES})

target_link_libraries(tictactoe SDL_helper SDL_ttf)
