/**
 * Simple SDL based tictactoe game; based off of SDL Tutorials.
 * 
 * Reference : http://www.sdltutorials.com/sdl-tutorial-tic-tac-toe
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#include "tictactoe.h"

void app::render() {
	/* Draw our surface using our common surface handling in the SDL_helper
	 * library. */
	surface::draw_surface(display, grid_img, 0, 0);
	// Render each of our cells in our grid accordingly.
	for(int i = 0; i < 9; i++) {
		/* Define X as 1 of 3 200 pixel cells laid out in a horizontal
		 * direction. */
		int x = (i % 3) * 200;
		// Define Y the same as X in a vertical direction.
		int y = (i / 3) * 200;
		// Draw either an X or O based on type in the above defined location.
		if(grid[i] == GRID_TYPE_X) {
			// Use the secondary function of OnDraw in the SDL_helper library.
			surface::draw_surface(display, x_img, x, y);
		} else if (grid[i] == GRID_TYPE_O) {
			surface::draw_surface(display, o_img, x, y);
		}
	}
	// Display a message (only if a game is not in progress).
	switch(game) {
		// Offset the text slightly for DRAW vs a Win.
		case DRAW:
			surface::draw_surface(display, txt, 206, 280);
			break;
		case X_WINS:
		case O_WINS:
			surface::draw_surface(display, txt, 212, 280);
			break;
		default:
			break;
	}

	SDL_Flip(display);
}
