/**
 * Simple SDL based map; based off of SDL Tutorials.
 * 
 * Reference : http://www.sdltutorials.com/sdl-maps
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#include "maps.h"

bool app::init() {
	if(SDL_Init(SDL_INIT_EVERYTHING) < 0) {
		return false;
	}
	if((display = SDL_SetVideoMode(WWIDTH, WHEIGHT, 32, SDL_HWSURFACE | SDL_DOUBLEBUF)) == NULL) {
		return false;
	}
	if(CArea::AreaControl.OnLoad("../res/maps/1.area") == false) {
		return false;
	}

	SDL_EnableKeyRepeat(1, SDL_DEFAULT_REPEAT_INTERVAL / 3);

	/* Finally return true, stating that all of our resources have properly
	 * initialized. */
    return true;
}
