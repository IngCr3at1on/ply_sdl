/**
 * Simple SDL based map; based off of SDL Tutorials.
 * 
 * Reference : http://www.sdltutorials.com/sdl-maps
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#include "maps.h"

void app::render() {
	CArea::AreaControl.OnRender(
		display,
		CCamera::CameraControl.GetX(),
		CCamera::CameraControl.GetY()
	);

	SDL_Flip(display);
}
