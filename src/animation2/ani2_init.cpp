/**
 * Base/template application for SDL Games.
 * 
 * Reference : http://www.sdltutorials.com/sdl-animation/
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#include "animation2.h"

bool app::init() {
	if(SDL_Init(SDL_INIT_EVERYTHING) < 0) {
		return false;
	}
	if((display = SDL_SetVideoMode(480, 500, 32, SDL_HWSURFACE | SDL_DOUBLEBUF)) == NULL) {
		return false;
	}
	if((image = surface::new_bitmap("../res/image/yoshi.bmp")) == NULL) {
		return false;
	/* Transparent is great but in this instance it doesn't quite work right, we
	 * need a solid color changer; which I added but it doesn't function. */
	} else if (surface::set_transparent(image, 255, 0, 255) == false) {
		return false;
	}
/* Returns true and the program runs but we still have pink.
 * 
	} else if (surface::change_solid_clr(image, 255, 0, 255, 0, 0, 0) == false) {
		/* Change pink to black using the new SolidColorChanger method.
		 * return false is unsuccessful. *
		 return false;
	}
*/
	if((image2 = surface::new_bitmap("../res/image/devil_droid.bmp")) == NULL) {
		return false;
	}

	Anim_Yoshi.MaxFrames = 8;

	/* Finally return true, stating that all of our resources have properly
	 * initialized. */
    return true;
}
