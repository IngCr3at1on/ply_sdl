/**
 * Simple SDL based tictactoe game; based off of SDL Tutorials.
 * 
 * Reference : http://www.sdltutorials.com/sdl-tutorial-tic-tac-toe
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#include "tictactoe.h"

bool app::init() {
    if(SDL_Init(SDL_INIT_EVERYTHING) < 0) {
        return false;
    }
    if((display = SDL_SetVideoMode(600, 600, 32, SDL_HWSURFACE | SDL_DOUBLEBUF)) == NULL) {
        return false;
    }
    /**
     * Load each of our images into memory, setting any transparency at the same
     * time (user our common surface handling within the SDL_helper library for
     * both of these functions).
     */
    if((grid_img = surface::new_bitmap("../res/image/grid.bmp")) == NULL) {
		return false;
	}
	if((x_img = surface::new_bitmap("../res/image/x.bmp")) == NULL) {
		return false;
	} else if (surface::set_transparent(x_img, 255, 0, 255) == false) {
		/* The SDL tutorial tells us to call the transparent function after our
		 * if statements; I've moved them inside of else statements.
		 * 
		 * The tutorial assumes the files are there and are going to load, but
		 * what if they don't? We're then calling transparent on a NULL image.*/
		return false;
	}
	if((o_img = surface::new_bitmap("../res/image/o.bmp")) == NULL) {
		return false;
	} else if (surface::set_transparent(o_img, 255, 0, 255) == false) {
		return false;
	}
	// Initialize TTF and a font for later usage.
	if(TTF_Init() < 0) {
		return false;
	}
	font = TTF_OpenFont("/usr/share/fonts/TTF/DejaVuSans.ttf",24);
	if(!font) {
		printf("TTF_OpenFont: %s\n", TTF_GetError());
		return false;
	}
	// We're using red.
	font_clr.r = 255;
	font_clr.g = 0;
	font_clr.b = 0;

	// Reset our board.
	reset();

	/* Finally return true, stating that all of our resources have properly
	 * initialized. */
	return true;
}
