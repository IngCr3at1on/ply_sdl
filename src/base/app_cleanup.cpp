/**
 * Base/template application for SDL Games.
 * 
 * Reference : http://www.sdltutorials.com/
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#include "app.h"

void app::cleanup() {
	SDL_FreeSurface(image);
	SDL_FreeSurface(display);

	SDL_Quit();
}
