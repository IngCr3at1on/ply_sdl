/**
 * Base/template application for SDL Games.
 * 
 * Reference : http://www.sdltutorials.com/sdl-animation/
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#include "single_animation.h"

void app::render() {
	surface::draw_surface(display, image, 0, 0, 0, Anim_Yoshi.GetCurrentFrame() * 64, 64, 64);

	SDL_Flip(display);
}
