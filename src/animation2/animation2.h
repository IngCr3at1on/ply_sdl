/**
 * Base/template application for SDL Games.
 * 
 * Reference : http://www.sdltutorials.com/sdl-animation/
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#ifndef _ANIMATION2_H_
#define _ANIMATION2_H_

#include <SDL/SDL.h>
// Get our helper services from a separate library.
#include "../../lib/SDL_helper/animate.h"
#include "../../lib/SDL_helper/event.h"
#include "../../lib/SDL_helper/surface.h"

class app : public CEvent {
	public:
		// Initialize our local/game variables.
		app() {
			display = NULL;
			image = NULL;
			image2 = NULL;

			running = true;
		}
		/**
		 * Primary / shell functionality (common to all/most games).
		 */
		// Start the main loop for the game itself.
		int execute();
		// Initialize all our resources (including SDL).
		bool init();
		// Process events through our SDL_helper common event handler.
		void handle_event(SDL_Event *event) {
			CEvent::OnEvent(event);
		}
			// Override our predefined (virtual) events.
			void OnExit();
		// Loop through basic animation.
		void loop() {
			Anim_Yoshi.OnAnimate();
		}
		// Render our final display.
		void render();
		// Cleanup/free any resources.
		void cleanup();

		/**
		 * Local functions.
		 */
		animate Anim_Yoshi;

	private:
		/**
		 * Primary / shell functionality (common to all/most games).
		 */
		// Track program status.
		bool running;
		// Primary display.
		SDL_Surface *display;
		/**
		 * Local functions.
		 */
		// A couple of surfaces.
		SDL_Surface *image;
		SDL_Surface *image2;
};

#endif // _ANIMATION2_H_
