/**
 * Simple SDL based tictactoe game; based off of SDL Tutorials.
 * 
 * Reference : http://www.sdltutorials.com/sdl-tutorial-tic-tac-toe
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#include "tictactoe.h"
/**
 * General overrides.
 */
// Basic close function.
void app::OnExit() {
	running = false;
}

// Capture left mouse click
void app::OnLButtonDown(int mX, int mY) {
	// If game state is not CONT then set it so that it is with this click.
	if(game != CONT) {
		game = CONT;
		return;
	}
	// Set our cell ID to one of our 200 pixel cells based on mouse location.
	int ID = mX / 200 + ((mY / 200) * 3);
	// If the cell has already been set return (needs proper error handling).
	if(grid[ID] != GRID_TYPE_NONE) {
		return;
	}
	// Alternate the cell/grid type based on the current players turn.
	if(cur_player == 0) {
		set_cell(ID, GRID_TYPE_X);
		cur_player = 1;
	} else {
		set_cell(ID, GRID_TYPE_O);
		cur_player = 0;
	}
	// Check the board for win/lose/draw conditions.
	int ret;
	ret = winlosedraw();
	switch(ret) {
		case X_WINS:
			// A player won so reset the board.
			reset();
			/* Set the game to ret to both require a player to click to start
			 * another game as well as control our text offset. */
			game = ret;
			// Write a message to the display.
			txt = TTF_RenderText_Solid(font, "Player X Wins!", font_clr);
			break;
		case O_WINS:
			reset();
			game = ret;
			txt = TTF_RenderText_Solid(font, "Player O Wins!", font_clr);
			break;
		case DRAW:
			reset();
			game = ret;
			txt = TTF_RenderText_Solid(font, "Game is a draw.", font_clr);
			break;
		default:
			break;
	}
}
