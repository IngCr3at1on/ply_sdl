/**
 * SDL collision exercise; based off of SDL Tutorials.
 * 
 * Reference : http://www.sdltutorials.com/sdl-collision
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#include "collision.h"
/**
 * General overrides.
 */
// Basic close function.
void app::OnExit() {
	running = false;
}

void app::OnKeyDown(SDLKey sym, SDLMod mod, Uint16 unicode) {
	switch(sym) {
		case SDLK_LEFT:
			player.mLeft = true;
			break;
		case SDLK_RIGHT:
			player.mRight = true;
			break;
		default:
			break;
	}
}
 
void app::OnKeyUp(SDLKey sym, SDLMod mod, Uint16 unicode) {
	switch(sym) {
		case SDLK_LEFT:
			player.mLeft = false;
			break;
		case SDLK_RIGHT:
			player.mRight = false;
			break;
		default:
			break;
	}
}
