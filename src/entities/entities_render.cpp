/**
 * Base/template application for SDL Games.
 * 
 * Reference : http://www.sdltutorials.com/sdl-entities
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#include "entities.h"

void app::render() {
	// Loop through each entity in entity list and run it's render method.
	for(int i = 0; i < entity::EntityList.size(); i++) {
		if(!entity::EntityList[i]) continue;

		entity::EntityList[i]->render(display);
	}

	SDL_Flip(display);
}
