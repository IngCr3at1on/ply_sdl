/**
 * SDL collision exercise; based off of SDL Tutorials.
 * 
 * Reference : http://www.sdltutorials.com/sdl-collision
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#include "collision.h"

void app::cleanup() {
	CArea::AreaControl.OnCleanup();
	SDL_FreeSurface(display);

	SDL_Quit();
}
