/**
 * Simple SDL based tictactoe game; based off of SDL Tutorials.
 * 
 * Reference : http://www.sdltutorials.com/sdl-tutorial-tic-tac-toe
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#include "tictactoe.h"

void app::cleanup() {
	SDL_FreeSurface(grid_img);
	SDL_FreeSurface(x_img);
	SDL_FreeSurface(o_img);
	SDL_FreeSurface(txt);

	SDL_FreeSurface(display);

	TTF_Quit();
	SDL_Quit();
}
