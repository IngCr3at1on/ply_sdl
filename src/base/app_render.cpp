/**
 * Base/template application for SDL Games.
 * 
 * Reference : http://www.sdltutorials.com/
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#include "app.h"

void app::render() {
	surface::draw_surface(display, image, 0, 0);

	SDL_Flip(display);
}
