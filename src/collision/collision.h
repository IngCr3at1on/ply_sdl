/**
 * SDL collision exercise; based off of SDL Tutorials.
 * 
 * Reference : http://www.sdltutorials.com/sdl-collision
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#ifndef _COLLISION_H_
#define _COLLISION_H_

#include <SDL/SDL.h>
// Get our helper services from a separate library.
#include "../../lib/SDL_helper/event.h"
#include "../../lib/SDL_helper/player.h"
#include "../../lib/SDL_helper/surface.h"
// Do the same w/ the area helper.
#include "../../lib/SDL_helper/area/area_helper.h"

class app : public CEvent {
	public:
		// Initialize our local/game variables.
		app() {
			display = NULL;

			running = true;
		}
		/**
		 * Primary / shell functionality (common to all/most games).
		 */
		// Start the main loop for the game itself.
		int execute();
		// Initialize all our resources (including SDL).
		bool init();
		// Process events through our SDL_helper common event handler.
		void handle_event(SDL_Event *event) {
			CEvent::OnEvent(event);
		}
			// Override our predefined (virtual) events.
			void OnExit();
			void OnKeyDown(SDLKey sym, SDLMod mod, Uint16 unicode);
			void OnKeyUp(SDLKey sym, SDLMod mod, Uint16 unicode);

		// Render our final display.
		void render();
		// Cleanup/free any resources.
		void cleanup();

		/**
		 * Local functions.
		 */
		Player player;
		Player player2;

	private:
		/**
		 * Primary / shell functionality (common to all/most games).
		 */
		// Track program status.
		bool running;
		// Primary display.
		SDL_Surface *display;
};

#endif // _COLLISION_H_
