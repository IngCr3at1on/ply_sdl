/**
 * Base/template application for SDL Games.
 * 
 * Reference : http://www.sdltutorials.com/
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#ifndef _APP_H_
#define _APP_H_

#include <SDL/SDL.h>
// Get our helper services from a separate library.
#include "../../lib/SDL_helper/event.h"
#include "../../lib/SDL_helper/surface.h"

class app : public CEvent {
	public:
		// Initialize our local/game variables.
		app() {
			display = NULL;
			image = NULL;

			running = true;
		}
		/**
		 * Primary / shell functionality (common to all/most games).
		 */
		// Start the main loop for the game itself.
		int execute();
		// Initialize all our resources (including SDL).
		bool init();
		// Process events through our SDL_helper common event handler.
		void handle_event(SDL_Event *event) {
			CEvent::OnEvent(event);
		}
			// Override our predefined (virtual) events.
			void OnExit();

		// Render our final display.
		void render();
		// Cleanup/free any resources.
		void cleanup();

	private:
		/**
		 * Primary / shell functionality (common to all/most games).
		 */
		// Track program status.
		bool running;
		// Primary display.
		SDL_Surface *display;
		/**
		 * Local functions.
		 */
		// A test surface.
		SDL_Surface *image;
};

#endif // _APP_H_
