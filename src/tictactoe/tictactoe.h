/**
 * Simple SDL based tictactoe game; based off of SDL Tutorials.
 * 
 * Reference : http://www.sdltutorials.com/sdl-tutorial-tic-tac-toe
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#ifndef _TICTAC_H_
#define _TICTAC_H_

#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
// Get our helper services from a separate library.
#include "../../lib/SDL_helper/event.h"
#include "../../lib/SDL_helper/surface.h"

class app : public CEvent {
	public:
		// Initialize our local/game variables.
		app() {
			display = NULL;

			grid_img = NULL;
			x_img = NULL;
			o_img = NULL;
			txt = NULL;

			running = true;
			/* Initialize the game as true so's not to require 2 clicks on the
			 * first move (related to win/lose/draw handling). */
			game = CONT;

			// Reset the standard game variables
			reset();
		}

		/**
		 * Primary / shell functionality (common to all/most games).
		 */
		// Start the main loop for the game itself.
		int execute();
		// Initialize all our resources (including SDL).
		bool init();
		// Process events through our SDL_helper common event handler.
		void handle_event(SDL_Event *event) {
			CEvent::OnEvent(event);
		}
			// Override our predefined (virtual) events.
			void OnExit();
			void OnLButtonDown(int mX, int mY);

		// Render our final display.
		void render();
		// Cleanup/free any resources.
		void cleanup();

		/**
		 * Local functions.
		 */
		// Set a grid cell to X or O.
		void set_cell(int ID, int Type);
		// Reset all grid cells.
		void reset();
		// Check the board for a win/lose/draw state.
		int winlosedraw();

	protected:
		/**
		 * Local functions.
		 */
		// Check a given row for a win/draw condition.
		int checkrow(int a, int b, int c);
		// Track if a row is filled.
		bool row0;
		bool row1;
		bool row2;
		bool row3;
		bool row4;
		bool row5;
		bool row6;
		bool row7;

	private:
		/**
		 * Primary / shell functionality (common to all/most games).
		 */
		// Track program status.
		bool running;
		// Track if a game is currently being played.
		int game;
		// Primary display.
		SDL_Surface *display;
		/**
		 * Local functions.
		 */
		// Display grid.
		SDL_Surface *grid_img;
		// Overlays.
		SDL_Surface *x_img;
		SDL_Surface *o_img;
		SDL_Surface *txt;
		// A true type font so we can write stuff.
		TTF_Font *font;
		// And a required font color.
		SDL_Color font_clr;
		// Player and grid/cell tracking.
		int cur_player;
		int grid[9];
		enum {
			GRID_TYPE_NONE,	// 0
			GRID_TYPE_X,	// 1
			GRID_TYPE_O		// 2
		};
		// Track the winner
		enum {
			DRAW,			// 0
			X_WINS,			// 1
			O_WINS,			// 2
			CONT			// 3
		};
};

#endif // _TICTAC_H_
