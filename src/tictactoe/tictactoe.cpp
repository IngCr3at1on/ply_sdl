/**
 * Simple SDL based tictactoe game; based off of SDL Tutorials.
 * 
 * Reference : http://www.sdltutorials.com/sdl-tutorial-tic-tac-toe
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#include "tictactoe.h"

int app::execute() {
	if(init() == false) {
		return -1;
	}

	SDL_Event event;

	while(running) {
		while(SDL_PollEvent(&event)) {
			handle_event(&event);
		}

		render();
	}

	cleanup();

	return 0;
}

// Set a given cell to either an X or O
void app::set_cell(int ID, int Type) {
	// Don't try to set a cell outside the grid.
	if(ID < 0 || ID >= 9) return;
	// Confirm we are setting the cell to a valid type.
	if(Type < 0 || Type > GRID_TYPE_O) return;
	
	grid[ID] = Type;
}

// Check a given row for a win/draw condition.
int app::checkrow(int a, int b, int c) {
	if((grid[a] == GRID_TYPE_X) && (grid[b] == GRID_TYPE_X) && (grid[c] == GRID_TYPE_X)) {
		// X wins!
		return X_WINS;
	} else if ((grid[a] == GRID_TYPE_O) && (grid[b] == GRID_TYPE_O) && (grid[c] == GRID_TYPE_O)) {
		// O wins!
		return O_WINS;
	} else {
		// Draw :(
		return DRAW;
	}
}

// Check the board for a win/lose/draw state.
int app::winlosedraw() {
	// Track the row to analyze.
	int row;
	/* Check each row to see if it is full and if it meets a win condition.
	 * Checking horizontal rows first, starting from the top (left to right). */
	// Track status/return for each row (full or not).
	if((grid[0] && grid[1] && grid[2]) != GRID_TYPE_NONE) {
		// If checkrow returns something other then 2 a player has won the game.
		if((row = checkrow(0, 1, 2)) != DRAW) {
			// Since a player won return row so we know who that is.
			return row;
		}
		// No player won but the row is still full.
		row0 = true;
	}
	if((grid[3] && grid[4] && grid[5]) != GRID_TYPE_NONE) {
		if((row = checkrow(3, 4, 5)) != DRAW) {
			return row;
		}
		row1 = true;
	}
	if((grid[6] && grid[7] && grid[8]) != GRID_TYPE_NONE) {
		if((row = checkrow(6, 7, 8)) != DRAW) {
			return row;
		}
		row2 = true;
	}
	// Now check our rows vertically starting from the left (top to bottom).
	if((grid[0] && grid[3] && grid[6]) != GRID_TYPE_NONE) {
		if((row = checkrow(0, 3, 6)) != DRAW) {
			return row;
		}
		row3 = true;
	}
	if((grid[1] && grid[4] && grid[7]) != GRID_TYPE_NONE) {
		if((row = checkrow(1, 4, 7)) != DRAW) {
			return row;
		}
		row4 = true;
	}
	if((grid[2] && grid[5] && grid[8]) != GRID_TYPE_NONE) {
		if((row = checkrow(2, 5, 8)) != DRAW) {
			return row;
		}
		row5 = true;
	}
	// Finally check both diagnal rows starting with the top left corner.
	if((grid[0] && grid[4] && grid[8]) != GRID_TYPE_NONE) {
		if((row = checkrow(0, 4, 8)) != DRAW) {
			return row;
		}
		row6 = true;
	}
	if((grid[2] && grid[4] && grid[6]) != GRID_TYPE_NONE) {
		if((row = checkrow(2, 4, 6)) != DRAW) {
			return row;
		}
		row7 = true;
	}
	// If all rows are full the game is a draw.
	if(row0 == true && row1 == true && row2 == true && row3 == true
		&& row4 == true && row5 == true && row6 == true && row7 == true) {
			return DRAW;
	} else {
		return CONT;
	}
}

// Clear our grid setting all the tiles to empty.
void app::reset() {
	for(int i = 0; i < 9; i++) {
		grid[i] = GRID_TYPE_NONE;
	}
	row0 = false;
	row1 = false;
	row2 = false;
	row3 = false;
	row4 = false;
	row5 = false;
	row6 = false;
	row7 = false;
	// A new game should always start w/ X
	cur_player = 0;
}

int main(int argc, char *argv[]) {
	app thisApp;

	return thisApp.execute();
}
