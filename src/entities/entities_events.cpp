/**
 * Base/template application for SDL Games.
 * 
 * Reference : http://www.sdltutorials.com/sdl-entities
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#include "entities.h"
/**
 * General overrides.
 */
// Basic close function.
void app::OnExit() {
	running = false;
}
