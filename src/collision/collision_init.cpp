/**
 * SDL collision exercise; based off of SDL Tutorials.
 * 
 * Reference : http://www.sdltutorials.com/sdl-collision
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#include "collision.h"

bool app::init() {
	if(SDL_Init(SDL_INIT_EVERYTHING) < 0) {
		return false;
	}
	if((display = SDL_SetVideoMode(WWIDTH, WHEIGHT, 32, SDL_HWSURFACE | SDL_DOUBLEBUF)) == NULL) {
		return false;
	}
	if(CArea::AreaControl.OnLoad("../res/maps/2.area") == false) {
		return false;
	}
	if(player.OnLoad("../res/image/yoshi.png", 64, 64, 8) == false) {
		return false;
	}
	if(player2.OnLoad("../res/image/yoshi.png", 64, 64, 8) == false) {
		return false;
	}

	player2.X = 100;

	entity::EntityList.push_back(&player);
	entity::EntityList.push_back(&player2);

	CCamera::CameraControl.mode = TARGET_MODE_CENTER;
	CCamera::CameraControl.SetTarget(&player.X, &player.Y);

	SDL_EnableKeyRepeat(1, SDL_DEFAULT_REPEAT_INTERVAL / 3);

    return true;
}
