/**
 * Base/template application for SDL Games.
 * 
 * Reference : http://www.sdltutorials.com/sdl-animation/
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#include "single_animation.h"

bool app::init() {
	if(SDL_Init(SDL_INIT_EVERYTHING) < 0) {
		return false;
	}
	if((display = SDL_SetVideoMode(64, 64, 32, SDL_HWSURFACE | SDL_DOUBLEBUF)) == NULL) {
		return false;
	}
	if((image = surface::new_bitmap("../res/image/yoshi.bmp")) == NULL) {
		return false;
	}
	if(surface::set_transparent(image, 255, 0, 255) == false) {
		return false;
	}
	Anim_Yoshi.MaxFrames = 8;

	/* Finally return true, stating that all of our resources have properly
	 * initialized. */
	return true;
}
