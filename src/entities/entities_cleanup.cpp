/**
 * Base/template application for SDL Games.
 * 
 * Reference : http://www.sdltutorials.com/sdl-entities
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#include "entities.h"

void app::cleanup() {
	// Loop through each of entities and run their contained cleanup methods.
	for(int i = 0; i < entity::EntityList.size(); i++) {
		if(!entity::EntityList[i]) continue;

		entity::EntityList[i]->cleanup();
	}
	entity::EntityList.clear();
	SDL_FreeSurface(display);

	SDL_Quit();
}
