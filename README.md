# ply_sdl
## Stands for Play SDL

Personal fiddlings w/ SDL tutorials (when I'm teaching myself something new I track it in git).

This repo was private, I set it as public to allow others to reference it:
None of the contained code should be considered as anything other than educational reference.

Requires SDL, SDL_ttf and cmake 2.6 or later.

The included submodule SDL_helper is also required to compile (fetch it with the following):

	git submodule init
	git submodule update

To compile examples perform an out-of-source build from the root directory of this repo.

	mkdir build
	cd build
	cmake ../
	make or make <subproject-name>

This will compile a binary file in build/src/<subproject>/<subproject-name> (where subproject and 
subproject-name are the project folder and name of the individual tutorials), they may be ran by 
issuing:

	./src/<subproject>/<subproject-name>

The contained cmake and .git files are also for educational reference and are subsequently more 
complex than the basic project should normally require.

