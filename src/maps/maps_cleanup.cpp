/**
 * Simple SDL based map; based off of SDL Tutorials.
 * 
 * Reference : http://www.sdltutorials.com/sdl-maps
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#include "maps.h"

void app::cleanup() {
	CArea::AreaControl.OnCleanup();
	SDL_FreeSurface(display);

	SDL_Quit();
}
