/**
 * SDL collision exercise; based off of SDL Tutorials.
 * 
 * Reference : http://www.sdltutorials.com/sdl-collision
 * 
 * Note: The following code may have been modified from the original tutorial. 
 * 
 * SDL Tutorials should not be held responsible for any of the contained code.
 */
#include "collision.h"

int app::execute() {
	if(init() == false) {
		return -1;
	}

	SDL_Event event;

	while(running) {
		while(SDL_PollEvent(&event)) {
			handle_event(&event);
		}

		render();
	}

	cleanup();

	return 0;
}

int main(int argc, char *argv[]) {
	app thisApp;

	return thisApp.execute();
}
